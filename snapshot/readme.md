### Snapshot util
##  Usage
Simple python app which would monitor your system/server. Output will be written to json file and stdout.

For monitoring purposes uses **psutil** module, see: https://pypi.org/project/psutil/ 

It creates snapshots of the state of the system each 30 seconds (configurable):

    {"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
    "%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
    "KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
    "KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
    "Timestamp": 1624400255}

The script can accept an interval (default value = 30 seconds) and output file name

    - "-i" Interval between snapshots in seconds, default=30)
    - "-f" Output file name, default="snapshot.json"
    - "-n" Quantity of snapshot to output, default=20

 
##  Installation
    cd ..
    pip install -U ./snapshot-util

