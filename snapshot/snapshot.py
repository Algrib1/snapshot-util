import psutil
import time
import json
import argparse
import os


class Sysinfo:

    def __init__(self):
        tasks = self.taskinfo()
        cpu = self.cpuinfo()
        mem = self.meminfo()
        self.log_dict = {
            "Tasks": {
                "total": tasks['total'],
                "running": tasks['running'],
                "sleeping": tasks['sleeping'],
                "stopped": tasks['stopped'],
                "zombie": tasks['zombie']
            },
            "%CPU": {
                "user": cpu['user'],
                "system": cpu['system'],
                "idle": cpu['idle']
            },
            "KiB Mem": {
                "total": mem['total'],
                "free": mem['free'],
                "used": mem['used']
            },
            "KiB Swap": {
                "total": mem['v_total'],
                "free": mem['v_free'],
                "used": mem['v_used']
            },
            "Timestamp": int(time.time())
        }

    def taskinfo(self):
        task_total = task_running = 0
        task_sleeping = task_stopped = task_zombie = 0
        for proc in psutil.process_iter():
            if proc.status() == 'running':
                task_running += 1
            elif proc.status() == 'sleeping':
                task_sleeping += 1
            elif proc.status() == 'stopped':
                task_stopped += 1
            elif proc.status() == 'zombie':
                task_zombie += 1
            task_total += 1
        task_dict = {
            'total': task_total,
            'running': task_running,
            'sleeping': task_sleeping,
            'stopped': task_stopped,
            'zombie': task_zombie
        }
        return task_dict

    def tokibi(self, x):
        """Convert bytes to KiB."""
        return x / 1024

    def meminfo(self):
        memo = psutil.virtual_memory()
        v_memo = psutil.swap_memory()
        mem_dict = {
            'total': int(self.tokibi(memo.total)),
            'free': int(self.tokibi(memo.free)),
            'used': int(self.tokibi(memo.used)),
            'v_total': int(self.tokibi(v_memo.total)),
            'v_free': int(self.tokibi(v_memo.free)),
            'v_used': int(self.tokibi(v_memo.used))
        }
        return mem_dict

    def cpuinfo(self):
        cpu = psutil.cpu_times_percent(interval=1, percpu=False)
        cpu_dict = {
            'user': cpu.user,
            'system': cpu.system,
            'idle': cpu.idle
        }
        return cpu_dict

    def json_out(self):
        return self.log_dict


def remove_if_exists(filename):
    if os.path.isfile(filename):
        os.remove(filename)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i",
                        help="Interval between snapshots in seconds",
                        type=int,
                        default=30)
    parser.add_argument("-f",
                        help="Output file name",
                        default="snapshot.json")
    parser.add_argument("-n",
                        help="Quantity of snapshot to output",
                        type=int,
                        default=20)
    args = parser.parse_args()

    remove_if_exists(args.f)

    with open(args.f, "a") as file:
        for num in range(args.n):
            system_info = Sysinfo()
            os.system('clear')
            json_output = json.dumps(system_info.json_out())
            print(system_info.json_out(), end="\r")
            file.write(json_output)
            file.write("\n")
            time.sleep(args.i)


if __name__ == "__main__":
    main()
