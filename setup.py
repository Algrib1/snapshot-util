from setuptools import setup, find_packages

setup(
    name="snapshot",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    install_requires=[
        'psutil'  # put your requirements separated by comma here
    ],
    version="0.1",
    author="Aliaksandr Hrybovich",
    author_email="aliaksandr_hrybovich@epam.com",
    description="Snapshot of system info",
    license="MIT",
)
